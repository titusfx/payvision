
export class Transaction {

    constructor(orderId, dealId, email, address, city, state, zipCode, creditCard){
        
        this.orderId = parseInt(orderId)
        this.dealId = parseInt(dealId)
        this.email = Transaction.cleanEmail(email)
        this.address = Address.cleanAddress(address)
        this.city = Address.cleanCity(city)
        this.state = Address.cleanState(state)
        this.zipCode = Address.cleanZipCode(zipCode)
        this.creditCard = this.cleanCreditCard(creditCard)
    }

     isFraudulentMatch(transaction){
        return this.dealId == transaction.dealId && this.creditCard != transaction.creditCard &&
                    (this.email == transaction.email || 
                        (this.address == transaction.address && this.city == transaction.city && this.state == transaction.state && this.zipCode == transaction.zipCode)
                )
    }

    static cleanEmail(email){

        var aux = email.toLowerCase().split('@');
        var username = aux[0];
        var domain = aux[1];

        return username.replace('.', '').split('+')[0] + '@' + domain;
    }

    static cleanCreditCard(creditCard){
        
        return parseInt(creditCard)
    }


}




class States{
        
    states = {
        'il': 'illinois',
        'ca': 'california',
        'ny': 'new york'
    }


    
}

class Address{
    
    static cleanAddress(address){

        return Address.deleteAbbreviations(address).replace('.', '');
    }

    
    static cleanCity(city){
        return city.toLowerCase();
    }

    static cleanState(state){
            return state.toLowerCase() in States.states ? States.states[state.toLowerCase()] : state.toLowerCase();
    }

    static cleanZipCode(zipCode){

        return parseInt(zipCode)
    }

    
    static deleteAbbreviations(address){

        var standardAddress = address.toLowerCase();
        
        addressAbbreviation = {'st.': 'street', 'rd.': 'road'};
        for(var abb in addressAbbreviation)
            standardAddress = standardAddress.replace(abb, addressAbbreviation[abb]);

        return standardAddress;
    }

}




